# This install script is now abandoned, I have switched distro (again) and now I use Gentoo, I've no time to keep this upto date.

## arch-post-install

A Post-install script for base Arch Linux installation to use with my [dotfiles](https://codeberg.org/codingotaku/dotfiles).

To install, run the following command in a terminal of your archlinux system.

```bash
curl -Lks https://codeberg.org/codingotaku/arch-post-install/raw/branch/main/install | /bin/bash
```

## What does this install?

The script installs and configures all the packages in the [packages.csv](packages.csv) file.
